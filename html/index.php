<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mutant Detector</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
    <header>
        <div class="navbar navbar-dark bg-dark shadow-sm">
            <div class="container d-flex justify-content-between">
                <a href="/" class="navbar-brand d-flex align-items-center">
                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAQAAABKfvVzAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QAAKqNIzIAAAAJcEhZcwAADdcAAA3XAUIom3gAAAAHdElNRQfiCwcWJQCNBxakAAACHklEQVQ4y7WSwUvTYRjHP+/PMRLmROeQEYuxCF2gBE6QIBDbyMPyFgxEEoLu3jx09NYfkDpB7CRFp1oONDBTiGbsEmJIEyRU/DULxSEYfjv8frrpunToeU/v9/l8n+d5Xx7DhdAV7hKniziwymdWeWeOqwlzAe9mhkM+kCcPdNPNHRp4aPLUhrwaU0nDNfqwShqTt9Ywo0WF+VupsBY1c1kc0JYaQX3yX9D96gM1aksD1XJA20qCvDrRniYck/ya0J5O5AUlta0AgAXAKFkzD/wG4vjIqUEN5PARd1QzT5bRSocV9YMsDUpqBWW0rGVlQK2SBmWB+rVyhtfpSEE1qaCChgBkaUELsgA0pIIKalJQR6pzRopRMjbjrJEgojlNkiRFiqQmNUeEBGuMG5sSMfAA7awrxD2ivOEb01zjOS+BBzxlgfu8JkVRIdZp54sH8HBKGxuEuWluA+gVb4Eeswm80C/CbNDGKR6nA0CZADv4FDVFMJvqAlMGUBQfOwQoO6DlrtQnIMYIS+oFMGUX72WJEWIu4f5SWjlQWrvqUEIH6jzPdOpACXVoV2lQTunKSJhZ+ZinjSLN57WaKZLnK0/M7JlkVW36FMeEanYvxLGZqlwt/jH+v8F5dL0iVbcaxs3WV5AePgLwE7sGtzl0s008cwziOxlsfmATuNTFQ4BHBGmhhcfIMWS5wVVuESRIkEb2z/F9rvMe2z3TZOEP7Gb2zHoVX40AAAAldEVYdGRhdGU6Y3JlYXRlADIwMTgtMTEtMDdUMjI6Mzc6MDArMDE6MDDO9lFfAAAAJXRFWHRkYXRlOm1vZGlmeQAyMDE4LTExLTA3VDIyOjM3OjAwKzAxOjAwv6vp4wAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAAASUVORK5CYII=" alt="">
                    <span class="pl-2"></span>
                    <strong>Mutant Detector</strong>
                </a>
            </div>
        </div>
    </header>

    <main role="main">
        <section class="jumbotron text-center">
            <div class="container">
                <h1 class="jumbotron-heading">Mutant Detector</h1>
                <p class="lead text-muted">More info in GitHub repository.</p>
                <p>
                    <a href="/mutant" class="btn btn-secondary my-2">/mutant</a>
                    <a href="/stats" class="btn btn-secondary my-2">/stats</a>
                    <a href="https://github.com/gbrlmza/magneto-mutant-detector" target="_blank" class="btn btn-primary my-2">GitHub Repo</a>
                </p>
            </div>
        </section>
    </div>
</body>
</html>