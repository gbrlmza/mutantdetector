# Mutant Detector

## Índice

- [Problema](#problema)
- [Criterios](#criterios)
- [Lenguaje y base de datos](#lenguaje-y-base-de-datos)
- [Soluciones](#soluciones)
  - [Fuerza bruta](#fuerza-bruta-stripbforcescanner)
  - [Fuerza bruta con expresiones regulares](#fuerza-bruta-con-expresiones-regulares-stripregexscanner)
  - [Máscaras binarias por cuadrantes](#máscaras-binarias-por-cuadrantes-cuadrantscannerphp)
  - [Performance / Spees tests](#performance-speedtest)
- [Web services](#web-services)
- [Instalación / Ejecución](#instalaciónejecución)
- [Tests](#tests)
- [Documentos](#documentos)

## Problema

Magneto quiere reclutar la mayor cantidad de mutantes para poder luchar contra los X-Mens.
Te ha contratado a ti para que desarrolles un proyecto que detecte si un humano es mutante basándose en su secuencia de ADN.
Para eso te ha pedido crear un programa con un método o función con la siguiente firma:

```
boolean isMutant(String[] dna);
```

En donde recibirás como parámetro un array de Strings que representan cada fila de una tabla de (NxN) con la secuencia del ADN. Las letras de los Strings solo pueden ser: (A,T,C,G), las cuales representa cada base nitrogenada del ADN. **Sabrás si un humano es mutante, si encuentras ​más de una secuencia de cuatro letras iguales, de forma oblicua, horizontal o vertical**.

**Mutante**

|||||||
|---|---|---|---|---|---|
|**A**|T|G|C|**G**|A|
|C|**A**|G|T|**G**|C|
|T|T|**A**|T|**G**|T|
|A|G|A|**A**|**G**|G|
|**C**|**C**|**C**|**C**|T|A|
|T|C|A|C|T|G|

**NO Mutante**

|||||||
|---|---|---|---|---|---|
| A | T | G | C | C | A |
| C | C | G | T | G | C |
| T | T | A | T | G | T |
| A | G | A | A | G | G |
| G | C | C | C | T | A |
| T | C | A | C | T | G |

### API REST
Crear una API REST, hostear esa API en un cloud computing libre (Google App Engine, Amazon AWS, etc), crear el servicio "/mutant/" en donde se pueda detectar si un humano es mutante enviando la secuencia de ADN mediante un HTTP POST con un Json el cual tenga el siguiente formato:

```
POST → /mutant/ {"dna":["ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"]}
```

En caso de verificar un mutante, debería devolver un HTTP 200-OK, en caso contrario un 403-Forbidden

### Estadísticas

Anexar una base de datos, la cual guarde los ADN’s verificados con la API. Solo 1 registro por ADN. Exponer un servicio extra "/stats" que devuelva un Json con las estadísticas de las verificaciones de ADN:

```
{"count_mutant_dna":40, "count_human_dna":100: "ratio":0.4}
```

Tener en cuenta que la API puede recibir fluctuaciones agresivas de tráfico (Entre 100 y 1 millón de peticiones por segundo).

Test-Automáticos, Code coverage > 80%, Diagrama de Secuencia / Arquitectura del sistema.

## Criterios

Ya que el problema es una mezcla de análisis secuencial de ADN con palabras cruzadas, tomo como criterio lo siguiente:

- No necesariamente el ancho y largo del ADN es el mismo.
- No hay restricción de letras, se considera mutante si las dos o más secuencias encontradas son de la misma o distintas letras.
- Dos secuencias pueden compartir 1 letra siempre y cuando tengan distinta dirección.
- No pueden haber dos secuencias consecutivas de la misma letra, es una sola secuencia larga.

**Mutante**

|||||||
|---|---|---|---|---|---|
|A|**G**|G|C|**G**|A|
|C|C|**G**|T|**G**|C|
|T|T|A|**G**|**G**|T|
|A|G|A|A|**G**|C|
|G|C|C|C|T|A|
|T|C|A|C|T|G|

**NO Mutante**

Es una sola secuencia de 8 caracteres y no puede ser considera como dos secuencias de 4.

||||||||||
|---|---|---|---|---|---|---|---|---|
|T|**A**|**A**|**A**|**A**|**A**|**A**|**A**|**A**|
|C|C|G|T|G|C|T|C|C|
|T|T|A|T|G|T|G|T|T|
|A|G|A|A|C|C|G|G|G|
|G|C|C|C|T|A|T|A|C|
|T|C|A|C|T|G|G|G|A|

## Lenguaje y base de datos

Realizo la implementación del algoritmo en PHP, teniendo en cuenta que Google Cloud utiliza OPcache no habrían problemas de performance. Selecciono este lenguaje por rapidez de implementación. En un escenario real utlizaría un lenguaje compilado en lo posible que pueda manejar la mayor cantidad posible de peticiones por segundo, con un servidor que permita múltiples peticiones por thread(non-blocking). Podría ser Go o Node.js con NGINX. Para el almacenamiento de datos se usa una instancia MySQL, donde junto con los datos del ADN se almacena un hash(sha1) del mismo que se utiliza para verificar si un ADN dado ya ha sido analizado. Se debería usar algún sistema de connection pooling para manejar una alta cantidad de peticiones.

TODO: Analizar posibilidad de colisiones con sha1 y evaluar otros algoritmos que produzan una salida de mayor cantidad de bits.

## Soluciones

Para la solución del problema utilicé dos enfoques. Uno tradicional de fuerza bruta donde se recorre el ADN de forma horizontal, vertical y oblicua hasta encontrar dos secuencias que cumplan las condiciones requeridas(más una subversión con expresiones regulares) y un método alternativo de máscaras binarias y cuadrantes. Debido a que la performance de la búsqueda por fuerza bruta con expresiones regulares fue mejor, se utiliza este método para la solución final.

### Fuerza bruta ([StripBforceScanner](src/Scanners/StripBforceScanner.php))

La lógica consiste en analizar el ADN en todas las direcciones posibles: horizontales, verticales y oblicuas. Para ello se toma una tira del ADN(puede correponder a cualquierda de las direcciones) y se recorre buscando una secuencia. El análisis se hace caracter por caracter, comparando cada caracter con el anterior y en caso de coincidir se incrementa un contador de logitud. En caso de diferir se comprueba la longitud de la secuencia y se reinicia el contador. Se pueden aplicar algunas micro optimizaciones como descartar las tiras oblicuas de menos de 4 caracteres de las esquinas, no analizar las primeras/últimas dos letra de cada tira en caso de que las dos letras siguientes/previas sean distintas o no buscar la última secuencia completa(solo las letras necesarias).

La cantidad de tiras que tiene un **ADN de X*Y** es `X + Y + 4X` que se simplifica a `Y + 5X`. Si el ADN tuviese el mismo ancho y alto **X*X** la cantidad de tiras se puede simplificar como `6X`.

### Fuerza bruta con expresiones regulares ([StripRegexScanner](src/Scanners/StripRegexScanner.php))

En la implementación final utilizo una expresión regular `/[A]{4,}|[G]{4,}|[T]{4,}|[C]{4,}/` para analizar tiras que en el caso particular de PHP es mucho más eficiente(PCRE, https://www.pcre.org/, desarrollado en C).

### Máscaras binarias por cuadrantes ([CuadrantScanner.php](src/Scanners/CuadrantScanner.php))

Como método alternativo utlicé una búsqueda de secuencias con máscaras binarias. Cómo la longitud mínima de la secuencia requerida es de 4 letras, planteé un análisis por cuadrante de 4x4. En un cuadrante de 4x4 hay 10 posibles casos de secuencias que cumplan la condición: 4 horizontales, 4 verticales y 2 oblicuas combinado con las letras posibles(ACGT) resultan en un total de 40 escenarios exitosos. Con este método se recorren todos los cuadrantes posibles del ADN. Dado un ADN de X*Y tendríamos `(X-3)*(Y-3)` cuadrantes. La comparación binaria AND entre el cuadrante y la máscara da como resultado la máscara si contiene la secuencia buscada y cualquier otro valor en caso contrario. Ej:

Cuadrante **AND Binario** Mascara **=** Resultado

|   |   |   |   |         |   |   |   |   |       |   |   |   |   |
|---|---|---|---|---------|---|---|---|---|-------|---|---|---|---|
| A | A | A | C |         | A | A | A | A |       | A | A | A |   |
| C | C | G | T | **AND** |   |   |   |   | **=** |   |   |   |   |
| T | T | A | T |         |   |   |   |   |       |   |   |   |   |
| A | G | A | A |         |   |   |   |   |       |   |   |   |   |

Si el resultado es igual a la máscara, el cuadrante contiene la secuencia buscada. Esta solución presenta un inconveniente(solucionado): Un secuencia encontrada en un cuadrante puede ser una continuación de una secuencia de otro cuadrante o la misma secuencia de otro cuadrante.

### Performance ([SpeedTest](utils/SpeedTest.php))

Realicé un análisis de velocidad en el entorno de desarrollo y si bien los tiempos varian de equipo en equipo sirve a modo comparativo entre los distintos metodos:

|**Método**  |**100000 ADN**|
|------------|------------------------|
|Regex       |2028ms                  |
|Fuerza Bruta|12293ms                 |
|Cuadrante   |17454ms                 |


La mayor velocidad del método regex se debe además de las optimizaciones que tenga, a que en el caso de PHP se usa el módulo PCRE (https://www.pcre.org/, desarrollado en C). En la implementación de los servicios web se usa el método con expresiones regulares.


## Web services

Para hostear los servicios utilicé Google Cloud, corriendo los servicios en PHP con MySQL.

- https://magneto-mutant-detector.appspot.com/
- https://magneto-mutant-detector.appspot.com/mutant
- https://magneto-mutant-detector.appspot.com/stats

## Instalación/Ejecución

### Docker

- Clonar repositorio `git clone https://github.com/gbrlmza/magneto-mutant-detector.git`
- Cambiar directorio `cd magneto-mutant-detector/`
- Ejecutar `./docker-run.sh`

El script por defecto expone el servidor web en el puerto **8081**. Modificar **docker-compose.yml** de ser necesario.

### Google Cloud SDK
Teniendo instalado GCloud SDK y app-engine-php(https://cloud.google.com/appengine/docs/standard/php/quickstart):

- Clonar repositorio `git clone https://github.com/gbrlmza/magneto-mutant-detector.git`
- Cambiar directorio `cd magneto-mutant-detector`
- Instalar dependencias `composer install`
- Inicializar base de datos con `util/database.sql`
- Copiar `config/app.dist.php` a `config/app.php` y ajustar configuración de DB.
- Ejecutar `dev_appserver.py app.yaml`

### Entorno PHP local

- Clonar repositorio `git clone https://github.com/gbrlmza/magneto-mutant-detector.git`
- Cambiar directorio `cd magneto-mutant-detector`
- Instalar dependencias `composer install`
- Inicializar base de datos con `util/database.sql`
- Copiar `config/app.dist.php` a `config/app.php` y ajustar configuración de DB.
- Cambiar directorio `cd html`
- Ejecutar `php -S localhost:8081`

## Tests

Ejecutar `./vendor/bin/phpunit tests/`

## Documentos

TODO: Agregar documentos funcionales/arquitectura.
