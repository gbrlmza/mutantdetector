<?php
namespace MutantDetector;
require(__DIR__.'/../vendor/autoload.php');

use PHPUnit\Framework\TestCase;

class ScannersTest extends TestCase
{
    public function testMutantDna()
    {
        $bforce = new Scanners\StripBforceScanner();
        $regex = new Scanners\StripRegexScanner();
        $cuadrant = new Scanners\CuadrantScanner();

        $dnas = [
            [
                'GTCG',
                'AGCG',
                'CTGG',
                'ATCG',
            ],
            [
                'GGGG',
                'ATCG',
                'ATCG',
                'ATCG',
            ],
            [
                "ATGAGA",
                "CAGTGC",
                "ACTTGT",
                "TACAAG",
                "CGCCTA",
                "AAAATG",
            ],
            [
                "TAAAAGCTACGTGGG",
                "TCCTGTAAATAGGTT",
                "CAGAATTTGCGTCGC",
                "TATGTAACAGCTAAA",
                "ATGTCCAGAAAACAA",
                "TGTCGTTTACTTTAT",
                "ACGTATTCTTATCAT",
                "AGACTACCTATTTAG",
                "GGTAGGGGAGTGTAC",
                "ACCCCTGTTACACAA",
                "GGTCTACCTATTTCG",
                "GCGACCCTCTCGAGT",
                "TGTTTGATGTGGCCC",
                "AGCTAGTCCGGCAAT",
                "CATTCTGGACGGAAA",
            ]
        ];
        
        foreach($dnas as $dna) {
            $result = $bforce->isMutant($dna);
            $this->assertTrue($result);
            
            $result = $regex->isMutant($dna);
            $this->assertTrue($result);

            $result = $cuadrant->isMutant($dna);
            $this->assertTrue($result);
        }
    }

    public function testHumanDna()
    {
        $bforce = new Scanners\StripBforceScanner();
        $regex = new Scanners\StripRegexScanner();
        $cuadrant = new Scanners\CuadrantScanner();

        $dnas = [
            [
                "ATCG",
                "ACCG",
                "CACG",
                "ATTG",
            ],
            [
                "ATGAGA",
                "CAGTGC",
                "ACTTGT",
                "TACAAG",
                "CGCCTA",
                "AACATG",
            ],
            [
                "TATAAGCTACGTGGG",
                "TCCTGTAAATACGTT",
                "CAGAATTTGCGTCGC",
                "TATGAAACAGCTAAA",
                "ATGTCCAGACAACAA",
                "TGCCGTCTACTTTAT",
                "ACGTATTCTTATCAT",
                "AGACTACCTATTTAG",
                "GGTAGTGGAGTGTAC",
                "ACCTCTGTTACACAA",
                "GGTCTACCTATTTCG",
                "GCGACCCTCTCGAGT",
                "TGTTTGATGTGGCCC",
                "AGCTAGTCCGGCAAT",
                "CATTCTGGACGGAAA",
            ]
        ];
        
        foreach($dnas as $dna) {
            $result = $bforce->isMutant($dna);
            $this->assertFalse($result);
            
            $result = $regex->isMutant($dna);
            $this->assertFalse($result);

            $result = $cuadrant->isMutant($dna);
            $this->assertFalse($result);
        }
    }
}