<?php
namespace MutantDetector;
require(__DIR__.'/../vendor/autoload.php');

use PHPUnit\Framework\TestCase;

class ValidatorsTest extends TestCase
{
    public function testValidatorFail()
    {
        // ADN con una fila de difetente longitud
        $data = [
            "dna" => [
                "ACGT",
                "ACGT",
                "ACGTT",
                "ACGT"
            ]
        ];
        $result = Data\Validators::isValidMutantRequest($data);
        $this->assertFalse($result);

        // ADN con caracteres no válidos
        $data = [
            "dna" => [
                "ACGT",
                "ACGT",
                "RRRR"
            ]
        ];
        $result = Data\Validators::isValidMutantRequest($data);
        $this->assertFalse($result);;
    }

    public function testValidatorSuccess()
    {
        $data = [
            "dna" => [
                "ATGAGA",
                "CAGTGC",
                "ACTTGT",
                "TACAAG",
                "CGCCTA",
                "AACATG",
            ]
        ];
        $result = Data\Validators::isValidMutantRequest($data);
        $this->assertTrue($result);
    }
}