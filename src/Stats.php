<?php
namespace MutantDetector;
require __DIR__.'/../vendor/autoload.php';
require __DIR__.'/../config/app.php';

/*
    @TODO: Las estadísticas se consultan con cada request lo cual no es óptimo.
    Se podrían usar los contadores atómicos de Memcached para llevar el
    conteo en memoria. La consulta a la base de datos se haría una sola
    vez y se volvería a repetir si expira la cache o se reinicia la instancia.
*/

$stats = Data\Database::getStats();
header('Content-Type: application/json');
echo json_encode($stats, JSON_PRETTY_PRINT);