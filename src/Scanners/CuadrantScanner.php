<?php
namespace MutantDetector\Scanners;

/**
 * Detectar ADN mutantes mediante análisis por cuadrantes.
 *
 * Planteé una enfoque alternativo para solucionar el problema de la búsqueda de secuencias, mediante
 * el análisis de cuadrantes con mapas binarios. En este método lo que hacemos es dividir el ADN en
 * cuadrante de 4x4 caracteres, y hacer una comparación binaria contra 40 cuadrantes que actuán como
 * filtros. Los cuarenta cuadrantes surgen de de las 10 posibles(4 horizontales, 4 verticales y
 * dos oblicuas) secuencias por los 4 caracteres.
 *
 * Teniendo un ADN de X*Y la cantidad de cuadrantes a analizar es (X-3)*(Y-3) y la cantidad total de
 * comparaciones es de (X-3)*(Y-3)*40.
 */
class CuadrantScanner
{
    // Tamaño de cuadrante. 4x4 = 16 celdas.
    CONST CUDRANT_SIZE = 4;

    /**
     * Mapeo de caracteres de ADN a caracteres que no produzcan colisiones con los operadores binarios, es
     * decir que el & binario entre dos caracteres no produzca otro caracter válido(Ej. G & A = A).
     *
     * A = 01000001 => A = 01000001
     * T = 01010100 => B = 01000010
     * C = 01000011 => D = 01000100
     * G = 01000111 => H = 01001000
     */
    CONST DNA_CHAR_CONVERSION = [
        'T' => 'B',
        'C' => 'D',
        'G' => 'H'
    ];

    /**
     * Determinar si el ADN dado es mutante.
     * Ej. $dna: ["ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"]
     * Retorna true o false, null en caso de error(tamaño de ADN menor a un cuadrante)
     *
     * @return mixed
     */
    public function isMutant(Array $dna)
    {
        // DNA Size
        $width = strlen($dna[0]);
        $height = count($dna);

        // Ambos lados menores a 4, imposible que sea mutante
        if ($width < self::CUDRANT_SIZE && $height < self::CUDRANT_SIZE) return false;

        // Uno de los lados menor a 4, no podemos analizarlo por cuadrante
        if ($width < self::CUDRANT_SIZE || $height < self::CUDRANT_SIZE) return null;

        $sdna = $this->serializeDna($dna);
        $patterns = $this->generatePatterns();
        $sequences = [];

        // Recorrer todos los cuadrantes del ADN
        $xlimit = $width - self::CUDRANT_SIZE;
        $ylimit = $height - self::CUDRANT_SIZE;
        for ($y0 = 0; $y0 <= $ylimit; $y0++) {
            for ($x0 = 0; $x0 <= $xlimit; $x0++) {

                $x1 = $x0 + self::CUDRANT_SIZE;
                $y1 = $y0 + self::CUDRANT_SIZE;

                // Obtener cuadrante como string de 16 caracteres
                $cudrant = '';
                for ($y = $y0; $y < $y1; $y++) {
                    for ($x = $x0; $x < $x1; $x++) {
                        $cudrant .= $sdna[$x + $y * $width];
                    }
                }

                // Comparar cuadrante contra patrones
                foreach ($patterns as $pattern => $data) {
                    $result = $cudrant & $pattern;

                    if ($result == $pattern) { // Secuencia válida
                        // Determinar posición global de la secuencia en base a la posición relativa al cuadrante
                        foreach($data[1] as &$coord) $coord = [$coord[0] + $x0, $coord[1] + $y0];

                        // Verificamos si es la continuación o misma secuencia de otro cuadrante
                        $existent = false;
                        foreach ($sequences as $seq) {
                            if ($seq[0] == $data[0] // Misma dirección
                                && $dna[$seq[1][0][1]][$seq[1][0][0]] == $dna[$data[1][0][1]][$data[1][0][0]] // Misma letra
                                && in_array([$data[1][0][0],$data[1][0][1]], $seq[1]) // Celda coincidente
                            ){
                                $existent = true;
                                break;
                            }
                        }
                        if ($existent) continue;

                        // Almacenamos secuencia
                        $sequences[] = $data;

                        // Si hay al menos dos secuencias, es mutante
                        if (count($sequences) >= 2) return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * Generar patrones binarios de comparación de secuencias para cuadrante de 4x4.
     * El resultado es la combinatoria de las posibilidades de secuencias de 4 caracteres iguales
     * por las 10 posibles posiciones en una grilla de 4x4(4 horizontales, 4 verticales, 2 oblicuas).
     *
     * El arreglo retornado tiene como key el patron de comparación y como valor un arreglo que tiene
     * en la key 0 el código de dirección y en la key 1 la secuencia de celdas.
     *
     * @return Array
     */
    private function generatePatterns()
    {
        $patterns = [];

        // Representación logica de secuencias de 4 caracteres iguales en un ADN de 4x4, dirección de 
        // la misma, y coordenadas relativas al cuadrante contenedor más una celda adicional siguiendo
        // el sentido.
        $mutantSequencesPatterns = [
            '1111000000000000' => ['H', [[0,0],[1,0],[2,0],[3,0],[4,0]]],
            '0000111100000000' => ['H', [[0,1],[1,1],[2,1],[3,1],[4,1]]],
            '0000000011110000' => ['H', [[0,2],[1,2],[2,2],[3,2],[4,2]]],
            '0000000000001111' => ['H', [[0,3],[1,3],[2,3],[3,3],[4,3]]],
            '1000100010001000' => ['V', [[0,0],[0,1],[0,2],[0,3],[0,4]]],
            '0100010001000100' => ['V', [[1,0],[1,1],[1,2],[1,3],[1,4]]],
            '0010001000100010' => ['V', [[2,0],[2,1],[2,2],[2,3],[2,4]]],
            '0001000100010001' => ['V', [[3,0],[3,1],[3,2],[3,3],[3,4]]],
            '1000010000100001' => ['OR', [[0,0],[1,1],[2,2],[3,3],[4,4]]],
            '0001001001001000' => ['OL', [[3,0],[2,1],[1,2],[0,3],[-1,4]]]
        ];

        // Generamos totalidad de combinaciones 4(caracteres) x 10(patrones) = 40
        foreach (['A','B','D','H'] as $char) {
            foreach ($mutantSequencesPatterns as $pattern => $data) {
                $pattern = str_replace('1', $char, $pattern);
                $pattern = str_replace('0', chr(0), $pattern);
                $patterns[$pattern] = $data;
            }
        }

        return $patterns;
    }

    /**
     * Convertir ADN a string con conversión de caracteres para operaciones binarias
     *
     * @return String
     */
    private function serializeDna(Array $dna)
    {
        $sdna = implode('', $dna);

        foreach (self::DNA_CHAR_CONVERSION as $from => $to) {
            $sdna = str_replace($from, $to, $sdna);
        }

        return $sdna;
    }
}