<?php
namespace MutantDetector\Scanners;

/**
 * Detectar ADN mutantes mediante análisis de tiras de ADN.
 */
class StripBforceScanner
{
    // Cantidad de caracteres iguales que debe tener una secuencia mutante
    CONST MIN_SEQUENCE_LENGTH = 4;

    // Cantidad de secuencias mutantes requeridas para calificar como mutante
    CONST REQUIRED_SEQUENCES = 2;

    /**
     * Determinar si el ADN dado es mutante
     * Ej. $dna: ["ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"]
     *
     * @return boolean
     */
    public function isMutant(Array $dna)
    {
        $sequences = $this->findSequences($dna);
        return ($sequences >= self::REQUIRED_SEQUENCES);
    }

    /**
     * Barrer ADN en búsqueda de secuencias.
     * En un solo barrido de celdas analizamos todas las tiras.
     * La cantidad de tiras en ADN de X*Y = X + Y + 4X quedando la fórmula final:
     * Tiras(ADN X*Y) = Y + 5X
     *
     * @param Array $dna ADN Formato: ["ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"]
     * @return Int  Cantidad de secuencias encontradas
     */
    private function findSequences(Array $dna)
    {
        $strips = [];
        $sequences = 0;
        $width = strlen($dna[0]);
        $height = count($dna);

        // Recorrer celdas por filas y armar tiras
        for ($y=0; $y<$height; $y++) {
            for ($x=0; $x<$width; $x++) {
                $letter = $dna[$y][$x]; // Letra de la celda actual

                // Armar tiras en las 4 posibles direcciones de cada celda
                foreach ([$y, "{$x}V", ($x-$y)."R", ($x+$y)."L"] as $key) {
                    if (!isset($strips[$key])) { // Tira vacía
                        $strips[$key] = $letter;
                    } elseif ($strips[$key][0] == $letter) { // Misma letra
                        $strips[$key] .= $letter;
                    } else { // Se encontró una letra distinta
                        // Analizamos secuencia armada hasta el momento
                        if (strlen($strips[$key]) >= self::MIN_SEQUENCE_LENGTH) $sequences++;
                        if ($sequences >= self::REQUIRED_SEQUENCES) return $sequences;
                        $strips[$key] = $letter; // Reiniciar tira con la nueva letra
                    }
                }
            }
        }

        // Analizamos tiras que no hayan tenido corte por cambio de letra
        foreach ($strips as $strip) {
            if (strlen($strip) >= self::MIN_SEQUENCE_LENGTH) $sequences++;
            if ($sequences >= self::REQUIRED_SEQUENCES) return $sequences;
        }

        return $sequences;
    }
}
