<?php
namespace MutantDetector\Scanners;

/**
 * Detectar ADN mutantes mediante análisis de tiras de ADN.
 */
class StripRegexScanner
{
    // Cantidad de caracteres iguales que debe tener un secuencia mutante
    CONST MIN_SEQUENCE_LENGTH = 4;

    // Cantidad de secuencias mutantes requeridas para calificar como mutante
    CONST REQUIRED_SEQUENCES = 2;

    /**
     * Determinar si el ADN dado es mutante
     * Ej. $dna: ["ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"]
     *
     * @param Array $dna ADN Formato: ["ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"]
     * @return boolean
     */
    public function isMutant(Array $dna)
    {
        $sequences = 0;

        // Tamaño de ADN
        $width = strlen($dna[0]);
        $height = count($dna);

        // Evaluamos tiras horizontales. Es el formato en el que recibimos el ADN
        if ($width >= self::MIN_SEQUENCE_LENGTH) {
            $this->scanStrips($dna, $sequences);
            if ($sequences >= self::REQUIRED_SEQUENCES) return true;
        }

        // Continuamos evaluando tiras verticales y oblicuas
        $strips = $this->getVerticalAndObliqueStrips($dna);
        $this->scanStrips($strips, $sequences);

        return ($sequences >= self::REQUIRED_SEQUENCES);
    }

    /**
     * Evaluar tiras de ADN en búsqueda de secuencias mutantes.
     *
     * @param Array $strips Array de stings con Tiras a analizar
     * @param Int   &$sequences Cantidad de secuencias mutantes totales.
     *              Por refencia, se actualiza este valor.
     */
    private function scanStrips($strips, &$sequences)
    {
        $regexStr = '[A]{#,}|[G]{#,}|[T]{#,}|[C]{#,}';
        $regexStr = str_replace('#', self::MIN_SEQUENCE_LENGTH, $regexStr);
        $regex = "/{$regexStr}/";

        foreach ($strips as $strip) {
            if (preg_match_all($regex, $strip, $matches)) {
                $sequences += count($matches[0]);
                if ($sequences >= self::REQUIRED_SEQUENCES) return;
            }
        }
    }

    /**
     * Obtener todas tiras verticales y oblicuas.
     * Se generan todas las tiras verticales y oblicuas en un solo barrido del ADN.
     *
     * @param Array $dna ADN Formato: ["ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"]
     * @return Array Arreglo con todas las tiras verticales y oblicuas
     */
    private function getVerticalAndObliqueStrips(Array $dna)
    {
        $width = strlen($dna[0]);
        $height = count($dna);

        // Inicializar keys de array contenedor de tiras.
        // Verticales, Oblicuas a la izquierda y Oblicuas a la derecha.
        $strips = array_merge(
            array_fill_keys(array_map(function($x){return "{$x}V";}, range(0, $width-1)), ''),
            array_fill_keys(array_map(function($x){return "{$x}R";}, range(-$width+1, $width-1)), ''),
            array_fill_keys(array_map(function($x){return "{$x}L";}, range(0, $width*2-2)), '')
        );

        // Recorrer celdas por filas y armar tiras
        for ($y=0; $y<$height; $y++) {
            for ($x=0; $x<$width; $x++) {
                $strips["{$x}V"] .= $dna[$y][$x]; // Vertical
                $strips[($x-$y)."R"] .= $dna[$y][$x]; // Oblicua hacia la derecha
                $strips[($x+$y)."L"] .= $dna[$y][$x]; // Oblicua hacia la izquierda
            }
        }

        // Filtrar tiras que no tengan la longitud mínima requerida
        $strips = array_filter($strips, function($x){return strlen($x) >= self::MIN_SEQUENCE_LENGTH;});

        return $strips;
    }
}