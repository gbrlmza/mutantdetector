<?php
namespace MutantDetector\Data;

class Validators
{
    /**
     * Verificar payload de detección mutante.
     */
    public static function isValidMutantRequest($payload)
    {
        if (!is_array($payload) || !isset($payload['dna'])) return false;
    
        $dna = $payload['dna'];
        if (!is_array($dna)) return false;
        if (!count($dna)) return false;

        // Validamos que todas las filas del ADN tengan el mismo ancho y caracteres válidos
        $width = null;
        $regex = '/^[CAGT]+$/';
        foreach ($dna as $row) {
            if (!is_string($row)) return false;
            $len = strlen($row);
            if ($width !== null && $len != $width) return false;
            $width = $len;
            if (!preg_match($regex, $row)) return false;            
        }

        return true;
    }
}