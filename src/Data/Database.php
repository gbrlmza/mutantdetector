<?php
namespace MutantDetector\Data;

class Database
{
    static $conn = null;

    /**
     * Obtener ADN por hash
     */
    public static function getDna($hash)
    {
        self::connect();
        $stmt = self::$conn->prepare("SELECT * FROM dnas WHERE hash = :hash LIMIT 1");
        $stmt->bindParam(':hash', $hash);
        $rs = $stmt->execute();
        return ($rs) ? $stmt->fetch() : null;
    }

    /**
     * Almacenar DNA analizado
     */
    public static function saveDna(Array $dna)
    {
        self::connect();
        $stmt = self::$conn->prepare("INSERT INTO dnas (hash,mutant,dna) VALUES (:hash,:mutant,:dna)");
        $stmt->bindParam(':hash', $dna['hash']);
        $stmt->bindParam(':mutant', $dna['mutant']);
        $stmt->bindParam(':dna', $dna['dna']);
        return $stmt->execute();
    }

    /**
     * Obtener Estadísticas
     */
    public static function getStats()
    {
        self::connect();
        $stats = self::$conn->query("SELECT count(*) as total, sum(mutant) as mutants FROM dnas")->fetch();
        $mutants = $stats['mutants'] * 1;
        $humans = $stats['total'] - $stats['mutants'];
        $ratio = ($mutants == 0 || $humans == 0) ? 0 : $mutants / $humans;
        return [
            "count_mutant_dna" => $mutants,
            "count_human_dna" => $humans,
            "ratio" => $ratio
        ];
    }

    /**
     * Conectar base de datos
     */
    static function connect()
    {
        if (!self::$conn) {
            self::$conn = new \PDO(MYSQL_DSN, MYSQL_USER, MYSQL_PASSWORD);
        }
    }
}