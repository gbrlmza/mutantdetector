<?php
namespace MutantDetector;
require __DIR__.'/../vendor/autoload.php';
require __DIR__.'/../config/app.php';

// Solo peticiones POST
if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
    http_response_code(405); // Method Not Allowed
    exit('Method not allowed. Only POST requests.');
}

// Extraer datos de payload. Solo tomamos ADN.
$payload = file_get_contents('php://input');
$data = json_decode($payload, true);
$data = array_intersect_key($data, ['dna'=>'']);
$jsonData = json_encode($data);

// Validar datos
if (!Data\Validators::isValidMutantRequest($data)) {
    http_response_code(400); // Bad Request
    exit('Bad request');
}

// Obtener ADN de base de datos
$hash = sha1($jsonData);
$dna = Data\Database::getDna($hash);

if ($dna) {
    // Existe el ADN en la DB, no hace falta analizarlo
    $isMutant = ($dna['mutant'] == 1);
} else {
    // Analizar ADN
    $scanner = new Scanners\StripBforceScanner();
    $isMutant = $scanner->isMutant($data['dna']);
    Data\Database::saveDna([
        'mutant' => $isMutant ? 1 : 0,
        'hash' => $hash,
        'dna' => $jsonData
    ]);
}

// Response
if ($isMutant) { // OK
    http_response_code(200);
    exit('Is mutant!');
} else { // Forbidden 
    http_response_code(403);
    exit('Not mutant!');
}