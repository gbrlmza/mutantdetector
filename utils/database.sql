DROP DATABASE IF EXISTS `mutantdb`;
CREATE DATABASE `mutantdb` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `mutantdb`;

DROP TABLE IF EXISTS `dnas`;
CREATE TABLE `dnas` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `hash` char(40) NOT NULL,
  `mutant` tinyint(4) NOT NULL DEFAULT '0',
  `dna` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dnas_hash` (`hash`),
  KEY `dnas_mutant` (`mutant`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;