<?php
namespace MutantDetector;
require(__DIR__.'/../vendor/autoload.php');

$scannerBforce = new Scanners\StripBforceScanner();
$scannerRegex = new Scanners\StripRegexScanner();
$scannerCuadrant = new Scanners\CuadrantScanner();

//========================================================================================
// Generar ADNs aleatorios
//========================================================================================
$dnas = [];
$mutants = [];
$count = 10000;
for ($i=0; $i < $count; $i++) {
    $dnas[] = randomDna();
}

//========================================================================================
// Medir StripRegexScanner 
//========================================================================================
$start = microtime(true) * 10000;
$mutant = 0;
foreach ($dnas as $key => $dna) {
    if ($scannerRegex->isMutant($dna)) {
        $mutant++;
        $mutants[$key] = $dna;
    }
}
$finish = microtime(true) * 10000;
$time = ceil(($finish - $start) / 10);
echo "Regex Time: {$time}ms. ADNs: {$count}. Mutant: {$mutant}\n";

//========================================================================================
// Medir StripBforceScanner
//========================================================================================
$start = microtime(true) * 10000;
$mutant = 0;
foreach ($dnas as $key => $dna) {
    if ($scannerBforce->isMutant($dna)) {
        $mutant++;
        $mutants[$key] = $dna;
    }
}
$finish = microtime(true) * 10000;
$time = ceil(($finish - $start) / 10);
echo "Bforce Time: {$time}ms. ADNs: {$count}. Mutant: {$mutant}\n";

//========================================================================================
// Medir CuadrantScanner 
//========================================================================================
$start = microtime(true) * 10000;
$mutant = 0;
foreach ($dnas as $key => $dna) {
    if ($scannerCuadrant->isMutant($dna)) {
        $mutant++;
        $mutants[$key] = $dna;
    }
}
$finish = microtime(true) * 10000;
$time = ceil(($finish - $start) / 10);
echo "Cuadrant Time: {$time}ms. ADNs: {$count}. Mutant: {$mutant}\n";

//========================================================================================
// Generar ADN aleatorio
//========================================================================================
function randomDna($size = null) {
    if (!$size) $size = random_int(4,15);
    $length = $size * $size;
    $characters = 'ATCG';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return str_split($randomString, $size);
}