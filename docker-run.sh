#!/bin/bash
docker-compose up -d
cp ./config/app.dist.php ./config/app.php
docker exec -it mutant-detector-app bash -c "cd /var/www && composer install"
